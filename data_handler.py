import pandas
import dateutil.parser as parser

class DataHandler:

	def __init__(self, csv_filename):
		self.df = pandas.read_csv(csv_filename)

	def _get_start_date(self, year, month):
		tmp = []
		tmp.append(year)
		tmp.append("-")
		tmp.append(month)
		tmp.append("-")
		tmp.append("01")
		start_date = ''.join(tmp)
		return start_date

	def _get_end_date(self, year, month):
		end_day = "31"

		if int(month) < 8:
			if int(month) % 2 == 0:
				end_day = "30"

			if int(month) == 2:
				end_day = "28"
				if (int(year) - 2012) % 4 == 0:
					end_day = "29"
		else:
			if int(month) % 2 != 0:
				end_day = "30"

		tmp = []
		tmp.append(year)
		tmp.append("-")
		tmp.append(month)
		tmp.append("-")
		tmp.append(end_day)
		end_date = ''.join(tmp)
		return end_date

	def get_dates_single(self, year, month):
		tmp = [self._get_start_date(year, month), self._get_end_date(year, month)]
		return tmp

	def get_dates_multi(self, s_year, s_month, e_year, e_month):
		tmp = [self._get_start_date(s_year, s_month), self._get_end_date(e_year, e_month)]
		return tmp

	def get_year_list(self):
		first_date = self.df.at[1, "Date"]
		last_date = self.df.at[len(self.df)-1, "Date"]
		first_year = parser.parse(first_date).year
		last_year = parser.parse(last_date).year
		tmp = []
		year = first_year
		while year <= last_year:
			tmp.append(str(year))
			year = year + 1
		return tmp

	# Filters out data based on dates and sorts according to 
	def get_amount(self, category, subcategory, start_date, end_date):
		df = self.df.loc[self.df["Date"].between(start_date, end_date)]
		df = df.loc[df['Category'] == category]
		if subcategory is not "All":
			df = df[df['Subcategory'] == subcategory]
		print(df)
		amount = df["Amount"].sum()
		return abs(round(amount*100)/100)
	
	