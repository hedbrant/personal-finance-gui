

class Definitions:

	def __init__(self):

		CLOTHING_SUBCAT = [
			"Clothes",
			"Jackets",
			"Other",
			"Shoes"
		]

		FOOD_SUBCAT = [
			"Alcohol",
			"Fast Food",
			"Groceries",
			"Ready Food",
			"Restaurant",
			"Snacks & Candy"
		]

		HOME_SUBCAT = [
			"Insurance",
			"Inventory",
			"Phone Bill",
			"Rent"
		]

		OTHER_SUBCAT = [
			"Other",
			"Uncategorized"
		]

		PERSONAL_SUBCAT = [
			"Creative",
			"Dos",
			"Excercise",
			"Experiences",
			"Gifts",
			"Hobby",
			"Investing",
			"Needs",
			"Parties",
			"Services",
			"Stuff",
			"Wants"
		]

		TRANSPORTATION_SUBCAT = [
			"Bike & Repair",
			"Car Services",
			"Commute",
			"Gas",
			"Parking"
		]

		TRAVELLING_SUBCAT = [
			"Accommodation",
			"Alcohol",
			"Combos",
			"Experiences",
			"Flights & Ground",
			"Food",
			"Other"
		]

		INCOME_SUBCAT = [
			"Salary",
			"Equities",
			"Account Transfer",
			"Part time work",
			"Other",
			"Uncategorized",
			"CSN"
		]

		self.CATEGORIES = [
			"Text",
			"Text"
		]

		self.CAT = [
			CLOTHING_SUBCAT, FOOD_SUBCAT, HOME_SUBCAT, OTHER_SUBCAT, PERSONAL_SUBCAT, TRANSPORTATION_SUBCAT, TRAVELLING_SUBCAT, INCOME_SUBCAT
		]

		self.MONTHS = [
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December"
		]