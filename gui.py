from tkinter import *
import pandas
from data_handler import DataHandler

class GUI:

	def __init__(self, data_handler, definitions):

		self.dh = data_handler
		self.defs = definitions

		self.root = Tk()
		self.root.title("EconGUI")
		self.root.geometry("1000x600")

		self._init_selectors()
		self._set_layout()

		self.root.mainloop()

	def _init_selectors(self):

		# --- Drop down selectors ---
		# From year
		self.from_year_sel = StringVar(self.root)
		self.from_year_sel.set(self.dh.get_year_list()[0])

		# From month
		self.from_month_sel = StringVar(self.root)
		self.from_month_sel.set(self.defs.MONTHS[0])

		# To year
		self.to_year_sel = StringVar(self.root)
		self.to_year_sel.set(self.dh.get_year_list()[len(self.dh.get_year_list())-1])

		# To month
		self.to_month_sel = StringVar(self.root)
		self.to_month_sel.set(self.defs.MONTHS[11])

		# Category
		self.cat_sel = StringVar(self.root)
		self.cat_sel.set(self.defs.CATEGORIES[0])

		# Subcategory
		self.subcat_sel = StringVar(self.root)

	def _set_layout(self):

		# Textblocks
		from_label = Label(self.root, text='From: ')
		from_label.grid(row=0, column=0)

		from_year_label = Label(self.root, text='Year: ')
		from_year_label.grid(row=1, column=0)

		from_month_label = Label(self.root, text='Month: ')
		from_month_label.grid(row=1, column=2)

		to_label = Label(self.root, text='To : ')
		to_label.grid(row=2, column=0)

		to_year_label = Label(self.root, text='Year: ')
		to_year_label.grid(row=3, column=0)

		to_month_label = Label(self.root, text='Month: ')
		to_month_label.grid(row=3, column=2)

		cat_label = Label(self.root, text='Category: ')
		cat_label.grid(row=4, column=0)

		subcat_label = Label(self.root, text='Subcategory: ')
		subcat_label.grid(row=6, column=0)

		# --- Dropdown menus ---
		# From year
		from_year_ddm = OptionMenu(self.root, self.from_year_sel, *self.dh.get_year_list())
		from_year_ddm.config(width=5)
		from_year_ddm.grid(row=1, column=1)

		# From month
		from_month_ddm = OptionMenu(self.root, self.from_month_sel, *self.defs.MONTHS)
		from_month_ddm.config(width=10)
		from_month_ddm.grid(row=1, column=3)

		# To year
		to_year_ddm = OptionMenu(self.root, self.to_year_sel, *self.dh.get_year_list())
		to_year_ddm.config(width=5)
		to_year_ddm.grid(row=3, column=1)

		# To month
		to_month_ddm = OptionMenu(self.root, self.to_month_sel, *self.defs.MONTHS)
		to_month_ddm.config(width=10)
		to_month_ddm.grid(row=3, column=3)

		# Choose category
		cat_ddm = OptionMenu(self.root, self.cat_sel, *self.defs.CATEGORIES)
		cat_ddm.config(width=10)
		cat_ddm.grid(row=5, column=0)

		# Choose category
		subcat_ddm = OptionMenu(self.root, self.subcat_sel, *self.defs.CATEGORIES)
		subcat_ddm.config(width=10)
		subcat_ddm.grid(row=5, column=0)


		# --- Buttons ---
		# Show button
		def show_btn_cmd():
			return
		show_btn = Button(self.root, text="Show", command=show_btn_cmd)
		show_btn.grid(row=8, column=0)


	
