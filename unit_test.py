import unittest
from data_handler import DataHandler

class TestGUI(unittest.TestCase):

	def setUp(self):
		csv_file = 'C:/Users/Olle/Documents/Python/EconomyGUI/expensemanager.csv'
		self.dh = DataHandler(csv_file)
	
	# DataHandler section
	"""def test_end_dates(self):
		months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
		for month in months:
			print(self.dh._get_end_date("2017", month))"""

	def test_get_dates(self):
		start_year = "2020"
		start_month = "05"
		end_year = "2024"
		end_month = "02"

		corr_dates_single = ["2020-05-01", "2020-05-31"]
		corr_dates_multi = ["2020-05-01", "2024-02-29"]
		self.assertTrue(self.dh.get_dates_single(start_year, start_month), corr_dates_single)
		self.assertTrue(self.dh.get_dates_multi(start_year, start_month, end_year, end_month), corr_dates_multi)
	
	def test_get_year_list(self):
		year_list = self.dh.get_year_list()
		self.assertListEqual(year_list, ["2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"])

if __name__ == "__main__":
	unittest.main()